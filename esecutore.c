#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>

/* Costanti */
#define MAXCMDSIZE 1024		//dimensione max di una riga
#define MAXCMD 10			//num massimo di comandi letti

/* Variabili globali */
int sequenziale = 0;		//flag modalità

/* Prototipi */
void setMod(char *mode);
int readCmd(char **bufptr);
char ** cmdSplitter(char * cmd);
int outputFile(int i);
int execCmd(char **cmdSplitted, int number);

/* MAIN */

int main(int argc, char *argv[])
{
	/* Leggo la modalita' */
	if(argc > 1){	//se non metto opzioni, di default eseguo in parallelo
		setMod(argv[1]);		
	}

	if(sequenziale){
		puts("Modalita' SEQUENZIALE");
	}else{
		puts("Modalita' PARALLELA");
	}
	
	/* Leggo i comandi da eseguire */
	char *cmdList[MAXCMD+1];
	int i = 0;
	cmdList[MAXCMD] = NULL;		//per terminare
	printf("Inserire la lista di comandi (max %d):\n",MAXCMD);
	while(readCmd(&cmdList[i])){
		if(cmdList[i]==NULL){puts("WARNING: termino l'acquisizione di comandi!\n"); break;}				//In caso di errore in lettura
		i++;																							//Incremento
		if(i==MAXCMD){ puts("WARNING: Raggiunto il massimo numero di comandi eseguibile!\n"); break; }	//Massimo raggiunto
	}

	/* Test: stampo i comandi letti! */
	i = 0;
	while(i<MAXCMD && cmdList[i]!=NULL){
		printf("%d^ comando inserito: %s\n",i+1,cmdList[i]);
		i++;
	}

	/* Splitto i comandi e gli stampo */
	char ** cmdSplitList[MAXCMD];
	i = 0;
	while(i<MAXCMD && cmdList[i]!=NULL){
		cmdSplitList[i] = cmdSplitter(cmdList[i]);
		i++;
	}

	/* eseguo i comandi */
	int state;
	i = 0;
	while(i<MAXCMD && cmdList[i]!=NULL){
		state = execCmd(cmdSplitList[i], i);
		i++;
	}
	
	return 0;
}


//Imposta la modalità in base all'opzione data
void setMod(char *mode){
	mode++;	//scorro al char dopo la -
	if(*mode == 's')
		sequenziale = 1;
}


//legge una riga dallo stdin, la salva in memoria dinamica e salva il ptr.
// Return:
// -1 -> Errore
//	0 -> Stringa vuota (fine lettura)
// >0 -> Dimensione riga letta
int readCmd(char **bufptr){
	char tmpbuf[MAXCMDSIZE];
	int ctrl = read(0, tmpbuf, MAXCMDSIZE-1);
	if(ctrl == 0){ //errori nella lettura
		perror("readCmd");
		*bufptr = NULL;
		return -1;
	}

	tmpbuf[ctrl-1] = '\0'; 	//tolgo lo \n finale

	if(strlen(tmpbuf)){		//se la stringa non è vuota
		*bufptr = (char *)malloc((strlen(tmpbuf)+1)*sizeof(char));	//creo un buffer su misura
		strcpy(*bufptr,tmpbuf);						//inserisco la stringa nel buffer
		return strlen(*bufptr);	//restituisco la lunghezza della stringa letta
	}else{
		*bufptr = NULL;
		return 0;
	}
}


//Splitta un comando dato e restituisce l'array con le reference
char ** cmdSplitter(char * cmd){
	int cmdSize = strlen(cmd);
	//Conto e sostituisco gli spazi:
	int i=0, nSpace=0;
	for(;i<cmdSize;i++){
		if(cmd[i]==' '){
			nSpace++;
			cmd[i]='\0';
		}
	}
	//Creo l'array di reference
	char **cmdSplitted = (char **)malloc((nSpace+2)*sizeof(char *));
	//Cerco e salvo i pezzi di comando
	cmdSplitted[0] = cmd;	//il primo è sicuro
	int j=1;	//scorre la splitList
	for(i=1;i<cmdSize;i++){
		if(cmd[i-1]=='\0' && cmd[i]!='\0'){
			cmdSplitted[j]=cmd+i;
			j++;
		}
	}
	cmdSplitted[j]=NULL;	//l'ultimo è null (come da specifiche)
	return cmdSplitted;
}


//Esegue un comando (splittato) dato. Return -1=errore; 1=ok
//TODO riunificare i punti di ritorno ove possibile
int execCmd(char **cmdSplitted, int number){
	int j=0;
	int fd=0;
	int PID=0;
	int returnValue=0;
	char errorStringO[1024];
	char errorStringE[1024];
	//crea file output
	fd=outputFile(number);
	if(fd==-1){
		return -1;
	}
	//redireziona stdout
	returnValue=dup2(fd, 1);
	if(returnValue == -1){
		sprintf(errorStringO, "\nerrore nel redirezionamento output comando %d: %d \n", number, errno);	
		perror(errorStringO);
		return -1;	
	}
	returnValue=dup2(fd, 2);
	if(returnValue == -1){
		sprintf(errorStringE, "\nerrore nel redirezionamento errore comando %d: %d \n", number, errno);
	       	perror(errorStringE);
      		return -1;
	}
	close(fd);	
	//fork
	PID=fork();
	if(!PID){
		//wait condizionale
		if(sequenziale){
			//ometto lo stato
			wait(NULL);
		}
	} else {
		//execvp
		execvp(cmdSplitted[0], &cmdSplitted[0]);
	}
	return 1;
}

//Prende in ingresso un intero (numero di processo inserito)
//e restituisce il fd del file per l'output
int outputFile(int i){
	char path[1024];
    	char errorString[1024];
	char removeString[1024];
	int rtn=-1;
	sprintf(path, "out.%d", i);
	sprintf(removeString, "rm -f out.%d", i);
	system(removeString);
	rtn=open(path, O_WRONLY|O_CREAT, 0666);
	if(rtn == -1){
		sprintf(errorString,"\n errore nella crezione del file: %d\n",errno);
		perror(errorString);
	}
	return rtn;
}
