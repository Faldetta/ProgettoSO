/* SCHEDULER DI PROCESSI */

//Dichiarazioni librerie
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

//Dichiarazioni Costanti
#define MAX_NAME 9		//MAX task name
//Tipi di ricerca task:
#define I_PRIORITY 0
#define I_EXEC 1
#define I_ID_P 2
#define I_ID_E 3	


//Dichiarazioni Variabili Globali
typedef struct task{
	int id;
	int priority;
	char name[9];
	int exec;
	struct task *next;
} Task;

Task *head = NULL;
Task *tail = NULL;
Task *garbage = NULL;
char policy = 'a';
int id_max = 0;


//Prototipi
int chooseOp();
int readInput(char *buf, int bufsize);
int readN();
//B
Task * createTask();
void formatName(char * name, int size);
int insertTask(Task * tsk);
Task ** searchInsert(Task **start, Task *tsk, int type);
void preInsert(Task **tskfound, Task *tsk);
Task * extractTask(Task **tskfound);
Task ** findID(int id);
//C
Task * recycleGarbage();
void collectGarbage(Task *tsk);
//D
void switchPolicy();
int execTask(Task *tsk);
int changePriority(int id, int newPriority);
//E
void printTable();
void printTask(Task * tsk);
//F
int loadTasks();
void printTableDebug();
void printTaskDebug(Task * tsk);



//MAIN
int main(int argc, char *argv[]){
	int loop = 1;
	while(loop){
		loop = chooseOp();	
	}	
    return 0;
}


/* FUNZIONI */

//CHOOSEOP: Stampa il menu e attende la scelta dell'utente
int chooseOp(){
	//Stampo il menu'
	puts("Operazioni disponibili:");	//puts appende automaticamente uno \n
	puts("1) Inserire un nuovo Task");
	puts("2) Eseguire il primo Task");
	puts("3) Eseguire un Task a scelta");
	puts("4) Eliminare un Task a scelta");
	puts("5) Modificare la priorita' di un Task");
	puts("6) Cambiare la politica di scheduling");
	puts("7) Uscire dal programma\n");

	//Scelta dell'utente
	printf("Inserire il numero dell'istruzione da eseguire:  ");
	fflush(stdout);					//necessario per far stampare, poichè la printf senza \n finale non fa svuotare il buffer dello stdout...
	int ch = readN();
	if(ch > 0){
		Task **tmpTaskPtr = &head;  //solo per inizializzare ad un valore diverso da NULL
		Task *tmpTask = head;		//solo per inizializzare ad un valore diverso da NULL
		switch(ch){
			case 1:	//CREA NUOVO TASK
				tmpTask = createTask();
				if(tmpTask != NULL){
					ch = insertTask(tmpTask);
					if(ch<0)
						puts("Impossibile inserire il task in lista!\n");
				}
				printTable();
				break;

			case 2:	//ESEGUI IL PRIMO TASK
				ch = execTask(tail);  
				if(ch<0){
					puts("Impossibile eseguire l'operazione!");
				}
				printTable();
				break;

			case 3:	//ESEGUI UN TASK A SCELTA
				printf("Inserire l'ID del task da eseguire: ");
				fflush(stdout);
				ch = readN();
				if(ch > 0){
					tmpTaskPtr = findID(ch);
					if(tmpTaskPtr != NULL){
						ch = execTask(*tmpTaskPtr);
					}
				}
				if(ch <=0 || tmpTaskPtr == NULL){ puts("Impossibile eseguire l'operazione!\n"); }	//segnalo eventuali errori			
				printTable();
				break;

			case 4:	//ELIMINA UN TASK A SCELTA
				printf("Inserire l'ID del task da eliminare: ");
				fflush(stdout);
				ch = readN();
				if(ch > 0){
					tmpTaskPtr = findID(ch);
					if(tmpTaskPtr != NULL){
						tmpTask = extractTask(tmpTaskPtr);
						collectGarbage(tmpTask);
					}	
				}
				if(ch <=0 || tmpTaskPtr == NULL){ puts("Impossibile eseguire l'operazione!\n"); }	//segnalo eventuali errori			
				printTable();
				break;

			case 5:	//MODIFICA LA PRIORITA' DI UN TASK
				printf("Inserire l'ID del task di cui si vuol modificare la priorita': ");
				fflush(stdout);
				ch = readN();
				printf("Inserire la nuova priorita' (0-9): ");
				fflush(stdout);
				int np = readN();
				if(ch<0 || np<0 || np>9){ 
					puts("Errore! Operazione annullata!\n"); 
				}else{
					ch = changePriority(ch,np);
					if(ch<0){ puts("Errore! Operazione annullata!"); }
				}
				printTable();
				break;

			case 6:	//CAMBIA LA POLITICA DI SCHEDULING
				switchPolicy();
				printTable();
				break;

			case 7:	//ESCI
				return 0;

			case 8:	//CARICA TASK DA FILE
				ch=loadTasks();
				if(ch<0){ puts("Errore nel caricamento!"); }
				printTableDebug();
				break;
			
			case 9:	//DEBUG
				printTableDebug();
				break;

			default:
				puts("Errore! Inserire un numero tra 1 e 7.\n");
				return 1;
		}
	}

	//Se l'operazione scelta è andata a buon fine, ricomincio:
	return 1;
}


//legge una stringa di una certa dimensione dallo stdin
int readInput(char *buf, int bufsize){
	char tmpbuf[1024];
	int ctrl = read(0, tmpbuf, 1023);
	if(ctrl == 0){ //errori nella lettura
		perror("readInput");
		return -1;
	}
	
	// fseek(0, 0, SEEK_END); 	//azzero il resto dello stdin	<---Genera segmentation fault (???)
	tmpbuf[ctrl-1] = '\0'; 	//tolgo lo \n finale

	//taglio la stringa:
	if(strlen(tmpbuf) >= bufsize-1){
		tmpbuf[bufsize-1]= '\0';		
	}

	strcpy(buf,tmpbuf);		//inserisco la stringa nel buffer dato

	//restituisco la lunghezza della stringa letta
	return strlen(buf);
}


//Legge e restituisce un intero (max 2 cifre)
int readN(){
	char ch_n[3]; 	//char 
	if(readInput(ch_n,3) < 0){ puts("Errore nella lettura!"); return -1;} //controllo eventuali fatal error

	int i, n=0;						//n = intero da ritornare
	for(i=0; i<strlen(ch_n); i++){	//prendo l'input un char alla volta
		
		//Controllo che il char inserito sia una cifra
		if(isdigit(ch_n[i])==0){ 
			puts("\nErrore! Inserire solo valori numerici!\n");
			return -1;
		}

		//Converto in intero:
		n *= 10;
		n += ch_n[i] - 48;
	}

	//ritorno l'intero
	return n;
}


/* B ***************************************************************************************/
//Crea un task
Task * createTask(){
	Task * tsk = recycleGarbage();	//se è possibile riciclo
	if(tsk == NULL)  				
		tsk = (Task *)malloc(sizeof(Task)); //se il garbage è vuoto creo un nuovo task
	

	//Ora riempio il task
	puts("\nCREAZIONE NUOVO TASK:");
	int tmp;

	//Assegno l'ID
	id_max += 1;
	tsk -> id = id_max;

	//Priorita'
	printf("Inserire la priorita' (tra 0 e 9): ");
	fflush(stdout);
	tmp = readN();
	if(tmp<0 || tmp>9){
		puts("\nErrore! Comando annullato!\n");
		collectGarbage(tsk);					//In caso di errore elimino il task
		return NULL;
	}else{
		tsk -> priority = tmp;
	}
	
	//Nome
	printf("Inserire il nome (max 8 caratteri): ");
	fflush(stdout);
	char c_tmp[MAX_NAME];
	tmp = readInput(c_tmp, MAX_NAME);
	formatName(c_tmp,tmp);		
	strcpy(tsk->name, c_tmp);

	//Esecuzioni
	printf("Inserire il numero di esecuzioni (tra 1 e 99): ");
	fflush(stdout);
	tmp = readN();
	if(tmp<1 || tmp>99){
		puts("\nErrore! Comando annullato!\n");
		collectGarbage(tsk);					//In caso di errore elimino il task
		return NULL;
	}else{
		tsk -> exec = tmp;
	}

	//Next
	tsk->next = NULL;

	return tsk;
}


//formatta il nome del task per la stampa e ne assegna uno di default se non è stato inserito
void formatName(char * name, int size){
	if(size == 0){	//assegno un nome di default
		char cn[3];
		strcpy(name,"def_");
		sprintf(cn, "%d", id_max);
		strcat(name,cn);
	}
	//Inserisco degli spazi alla fine del nome per aggiustare la formattazione
	int max = MAX_NAME-1;
	int n_space = max - strlen(name);
	while(n_space>0){
		name[max-n_space] = ' ';
		n_space--;
	}
	name[max]='\0';	//l'ultimo elemento è sempre EoS
}


//inserisce il task in lista, in ordine per policy
int insertTask(Task * tsk){
	if(head == NULL ){ //lista vuota
		head = tsk;
		tail = tsk;
		return 1;		//inserimento avvenuto con successo
	}

	//cerco il punto in cui effettuare l'inserimento:
	Task ** insertPoint;
	if(policy=='a'){
		insertPoint = searchInsert(&head, tsk, I_PRIORITY);	
	}else{
		insertPoint = searchInsert(&head, tsk, I_EXEC);
	}

	//inserisco
	if(insertPoint==NULL){ return -1; }; //errore
	
	preInsert(insertPoint,tsk);	

	if((*insertPoint)->next == NULL){ //Inserimento in coda
		tail = *insertPoint;
	}

	return 1;
}


//Ricerca il punto in cui inserire un task, in base alla priorità
Task ** searchInsert(Task **start, Task *tsk, int type){
	while((*start) != NULL){
		switch(type){
			case I_PRIORITY:
				if(tsk->priority >= (*start)->priority){
					if(tsk->priority == (*start)->priority){
						start = searchInsert(start,tsk,I_ID_P);
					}
					return start;
				}
				break;
			case I_EXEC:
				if(tsk->exec <= (*start)->exec){
					if(tsk->exec == (*start)->exec){
						start = searchInsert(start,tsk,I_ID_E);
					}
					return start;
				}
				break;
			case I_ID_P:
				if(tsk->priority != (*start)->priority){ return start; }
				if(tsk->id > (*start)->id){	
					return start;
				}
				break;
			case I_ID_E:
				if(tsk->exec != (*start)->exec){ return start; }
				if(tsk->id > (*start)->id){	
					return start;
				}
				break;
			default:
				puts("ERRORE! Tipo ricerca non specificato!");
				return NULL;
		}
		start = &((*start)->next);
	}
	return start;	
}


//Esegue un inserimento prima del task dato
void preInsert(Task **tskfound, Task *tsk){
	Task *tmp;
	tmp = *tskfound; 	//memorizzo l’indirizzo del nodo dato
	*tskfound = tsk;	//inserisco il nuovo nodo
	(*tskfound)->next = tmp; 	//collego al nuovo nodo il nodo dato
}


//Estrae il task puntato dal doppio puntatore e ripristina la lista. Ritorna il task estratto
Task * extractTask(Task **tskfound){
		Task *tsk = *tskfound;			//salvo il ptr del task da estrarre
		*tskfound = tsk->next; 			//scollego il task dalla lista

		//se estraggo in coda, aggiorno il ptr alla coda
		if(tsk == tail){		
			Task *tmp = head;
			while(tmp->next != NULL){
				tmp = tmp->next;
			}
			tail = tmp;
		}

		tsk->next = NULL;	//scollego dalla vecchia lista
		return tsk;
}


//Cerca un task con un certo id. Per comodità d'uso restituisce il doppio ptr al task
Task ** findID(int id){
	Task **ptrTask = &head;
	while(*ptrTask != NULL){
		if((*ptrTask)->id == id){
			return ptrTask;
		}
		ptrTask = &((*ptrTask)->next);
	}
	puts("Errore! ID non trovato!");
	return NULL;
}


/* C **************************************************************************************/
//Se il garbage non è vuoto restituisce un task da riciclare, altrimenti NULL
Task * recycleGarbage(){
	if(garbage == NULL) {return NULL;}
	//Estraggo un task da riciclare
	Task * tsk = garbage;
	//aggiorno il garbage
	garbage = garbage->next;
	return tsk;
}


//Inserisco un task nel garbage
void collectGarbage(Task *tsk){
	Task *tmp = garbage;
	garbage = tsk;
	tsk->next = tmp;
}

/* D **************************************************************************************/
//Cambia la politica e riordina la lista dei task
void switchPolicy(){								
	//cambio la politica
	if(policy=='a'){
		policy = 'b';
	}else{
		policy = 'a';
	}
	//svuoto la lista per riordinarla
	Task *oldH = head;
	head = NULL;
	//reinserisco tutti i task
	Task *tmpTask;
	while(oldH != NULL){
		tmpTask = oldH;
		oldH = oldH->next;
		tmpTask->next = NULL;
		insertTask(tmpTask);
	}
}


//Esegue un task dato
int execTask(Task *tsk){						
	//decremento il numero di esecuzioni
	(tsk->exec)--;

	Task * tmpTask;
	if(tsk->exec == 0){	//Task completato 
		tmpTask = extractTask(findID(tsk->id));
		collectGarbage(tmpTask);
		return 1;
	}

	//Controllo la politica
	if(policy == 'a'){ return 1; }	//Politica per priorita' -> no reorder

	//Politica per n-exec: estraggo il task e lo reinserisco
	tmpTask = extractTask(findID(tsk->id));
	int r = insertTask(tmpTask);

	if(r<0){ //gestione errori inserimento -> elimino il task
		collectGarbage(tsk);
		puts("Errore! Operazione annullata!");
		return -1;
	}

	return 1;
}


//Cambia la priorità di un task dato, eventualmente lo reinserisce ordinato
int changePriority(int id, int newPriority){
	Task **ptr = findID(id);
	if(ptr == NULL){ return -1; }
	(*ptr)->priority = newPriority;

	//Controllo la politica
	if(policy == 'b'){ return 1; }	//Politica per esecuzioni -> no reorder

	//Politica per priorita': estraggo il task e lo reinserisco
	Task * tmpTask = extractTask(ptr);
	int r = insertTask(tmpTask);

	if(r<0){ //gestione errori inserimento -> elimino il task
		collectGarbage(tmpTask);
		puts("Errore! Operazione annullata!");
		return -1;
	}

	return 1;

}


/* E **************************************************************************************/
//Stampa la tabella
void printTable(){
	puts("");
	puts("+----+-----------+-----------+-------------------+");
	puts("| ID | PRIORITA' | NOME TASK | ESECUZ. RIMANENTI |");
	puts("+----+-----------+-----------+-------------------+");
	Task *tsk = head;
	while(tsk!=NULL){
		printTask(tsk);
		tsk = tsk->next;
		puts("+----+-----------+-----------+-------------------+");
	}
	printf("La policy selezionata e': %c\n\n", policy);
}


//Stampa un task:
void printTask(Task * tsk){
	printf("| ");
	if(tsk->id < 10){ printf(" "); }	//aggiungo uno zero per formattazione
	printf("%d |     %d     | %s  |        ", tsk->id,tsk->priority, tsk->name);
	if(tsk->exec < 10){ printf(" "); }
	printf("%d         |\n", tsk->exec);
}


/* F **************************************************************************************/
//Carica una lista di task da file
int loadTasks(){
	FILE *fp = fopen("test.txt", "r");

	if (!fp)
    	return -1;

    char buf[1024];
    const char s[2] = "|";
    char *token;
    Task * tsk;
    int counter;
	while (fgets(buf,1024, fp)!=NULL){	//leggo riga per riga
		counter = 0;
		tsk = (Task *) malloc(sizeof(Task)); //creo un nuovo task
		token = strtok(buf,s);	//spezzo la riga in token
		//riempio i vari campi
		while(token != NULL){
			switch(counter){
				case 0:
					tsk->id = atoi(token);
					if(tsk->id > id_max){ id_max = tsk->id; }
					break;
				case 1:
					tsk->priority = atoi(token);
					break;
				case 2:
					strcpy(tsk->name,token);
					formatName(tsk->name, strlen(tsk->name));
					break;
				case 3:
					tsk->exec = atoi(token);
					break;
			}
			counter++;
			token = strtok(NULL, s);	//scorro alla prossima token
		}
		tsk->next = NULL;
		insertTask(tsk);	//inserisco in lista
	}

	fclose(fp);
	return 1;
}


//Stampa la tabella con informazioni di debug:
void printTableDebug(){
	puts("");
	puts("+----+-----------+----------+------+-----------+----------+----------+");
	puts("| ID | PRIORITA' |   NOME   | EXEC | POSIZIONE |   NEXT   | NEXT_POS |");
	puts("+----+-----------+----------+------+-----------+----------+----------+");
	Task *tsk = head;
	while(tsk!=NULL){
		printTaskDebug(tsk);
		tsk = tsk->next;
		puts("+----+-----------+----------+------+-----------+----------+----------+");
	}
	puts("+----------+----------+----------+--------+");
	puts("|   HEAD   |   TAIL   | GARBAGE  | POLICY |");
	puts("+----------+----------+----------+--------+");
	printf("| %p | %p | %p |   %c    |\n", head, tail, garbage, policy);
	puts("+----------+----------+----------+--------+");
}

	


//Stampa un task con info debug
void printTaskDebug(Task * tsk){
	printf("| ");
	if(tsk->id < 10){ printf(" "); }	//aggiungo uno spazio per formattazione
	printf("%d |     %d     | %s |", tsk->id,tsk->priority, tsk->name);
	if(tsk->exec < 10){ printf(" "); }
	printf("  %d  |", tsk->exec);
	printf(" %p  | %p | %p |\n", tsk,tsk->next, &(tsk->next));
}

