#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>
#include <ctype.h>

/* COSTANTI */
#define S_PIPE_NAME "serverIN"
#define BUF_SIZE 1024
#define MAX_ANS_PIECES	10	//Numero massimo di pezzi di una risposta

//Delimitatori
#define CMD_DELIM "|"			//Delimitatore token comandi
#define ID_DELIM ","			//delimitatore token id destinatari multipli
#define INFO_DELIM "_"			//Delimitatore informazioni client

/* Definizione Codici Comandi */
#define DENIAL 0
#define CONFIRM 1
#define CONNECT 2
#define LIST 3
#define FORWARD 4
#define DISCONNECT 5
#define KILL 9			//Test only

/* Definizione Segnali */
#define NEW_MEX SIGUSR1 		//Nuovo messaggio da leggere
#define SIG_UPDATE SIGUSR2		//Notifica aggiornamento clients


/* VARIABILI GLOBALI */
int server;				//fd server
char answer[BUF_SIZE];	//Buffer per alloggiare le risposte del server
char *ansSplitted[MAX_ANS_PIECES];	//Prende la posizione dei pezzi di risposta

//Info Client
char *myPipeName;		//Nome Pipe in ingresso
int myPipe;				//fd pipe in ingresso
char * myName;			//Nome Client (deciso dall'utente)
int myID = -1;			//Id assegnato dal server	

//Flags
int connected = 0;		//Indica se il client è connesso al server
int updates = 0;		//Indica se ci sono aggiornamenti sui client connessi


/* PROTOTIPI */
char * getClientName();
int createPipe();
void printMenu();
int chooseAction();
int readN();

//connect
int connectToServer();
void waitServer();
//list
void listRequest();
//term
void closeMyPipe();

int createCommandStr(char *str, int cod, char *dest, char *txt);
int sendRequest(char *req);
int readLine(int fd, char *str);
void splitAnswer();
void printMessage(char *mex);

void sendMessage();
void sendAnswer(char * id);
char * readIDs(char * dest);
char * readMex(char * dest);
void disconnect();

void incomingMessageHandler(int sig);
void updateHandler(int sig);
void killHandler(int sig);

void killAll();

/* MAIN */
int main(void){
	puts("CLIENT");
	// 1 - Prendo il nome del client
	myName = getClientName();
	// 2 - Creo la pipe in ingresso
	myPipe = createPipe();
	// 3 - Imposto i signal handlers
	signal(NEW_MEX, incomingMessageHandler);
	signal(SIG_UPDATE, updateHandler);
	signal(SIGINT, killHandler);
	// 4 - Ciclo di esecuzione
	printMenu();
	int action;		//operazione scelta	
	while(1){
		action=chooseAction();
		if(!connected){
			if(action == 1){ //CONNESSIONE
				while(!connectToServer()){      //Tento di connettermi finchè il server non è disponibile
	                        	sleep(1);
	                               	action++;
	                                printf("Tentativo di connessione %d^...\n",action);
	                        }
			}else if(action == 5) { //TERMINAZIONE
				 puts("Bye!");
				 closeMyPipe();
				 return 0;

			}else{  //FORZO CONNESSIONE AL SERVER
				printf("Necessaria connessione al Server\n");
				printf("Inserire l'operazione da eseguire: ");
				fflush(stdout);
			}
		}else{
			switch(action){
				case 1:	//CONNESSIONE
					puts("Errore! Already connected!");
					printf("Inserire l'operazione da eseguire: ");
					fflush(stdout);
					break;
				case 2:	//LIST
					puts("Richiedo lista utenti attivi...");
					listRequest();
					break;
				case 3: //INVIO MESSAGGIO
					sendMessage();
					break;
				case 4: //DISCONNESSIONE
					disconnect();
					break;
				case 5:	//TERMINAZIONE
					disconnect();
					puts("Bye!");
					closeMyPipe();
					return 0;
				case 6: //STAMPA MENU
					printMenu();
					break;
	
				case 9: //KILL_ALL (test only)
					killAll();
				default:
					puts("Errore! Inserire un numero compreso tra 1 e 6!");
					printf("Inserire l'operazione da eseguire: ");
					fflush(stdout);
			}
		}
	}
}


//Chiede e salva il nome del client
char * getClientName(){
	printf("Inserire il nome del client: ");
	fflush(stdout);
	char buf[BUF_SIZE];
	int ctrl = read(0, buf, 1023);			
	buf[ctrl-1] = '\0'; 				//Tolgo lo \n finale
	char * name = (char *)malloc((strlen(buf)+1)*sizeof(char));
	return strcpy(name,buf);
}


//Crea la pipe in ingresso per le risposte del server. Return fd
int createPipe(){
	//Creo un nome univoco
	char buf[BUF_SIZE];
	sprintf(buf,"pipe%d",getpid());
	myPipeName = (char *)malloc((strlen(buf)+1)*sizeof(char));
	strcpy(myPipeName,buf);
	//Creo la pipe:
	unlink(myPipeName);				//rimuove la pipe se già presente, per chiudere altre connessioni
	mknod(myPipeName,S_IFIFO, 0);			//creo la pipe
	chmod(myPipeName, 0666);			//modifico i permessi della pipe
	//apro la pipe in lettura (non bloccante, almeno non aspetto che qualcuno apra in scrittura)
	return open(myPipeName, O_RDONLY|O_NONBLOCK);	
}


//Stampa il menu
void printMenu(){
	puts("--Operazioni Disponibili--");
	puts(" 1 - Connessione al server");
	puts(" 2 - Richiesta lista utenti attivi");
	puts(" 3 - Invio di un messaggio testuale ad un altro client");
	puts(" 4 - Disconnessione dal server");
	puts(" 5 - Terminazione del programma");
	puts(" 6 - Ristampa menu'\n");
	printf("Inserire l'operazione da eseguire: ");
	fflush(stdout);
}


//Prende l'azione scelta
int chooseAction(){
	return readN();
}

//Legge un intero
int readN(){
	char buf[BUF_SIZE];
	int ctrl = read(0, buf, 1023);			
	buf[ctrl-1] = '\0'; 				//Tolgo lo \n finale
	if(!isdigit(*buf)){	puts("Errore, inserire un valore numerico!"); return -1;}		//errore
	return atoi(buf);
}


/* Operazioni */

/* CONNECT */
//Gestisce l'operazione di connessione al server. Ritorna 0 se il server non è disponibile.
int connectToServer(){
	// 1 - Preparo il messaggio da trasmettere
	char mex[BUF_SIZE];
	char info[BUF_SIZE];
	sprintf(info,"%d_%s_%s",getpid(),myName,myPipeName);	//Informazioni da trasmettere
	createCommandStr(mex, CONNECT, "-1", info);				//Creo il messaggio
	// 2 - Aspetto l'apertura delle comunicazioni con il server
	waitServer();
	// 3 - Invio il messaggio e attendo la risposta
	sendRequest(mex);
	// 4 - Elaboro la risposta letta dall'handler
	if(connected){	//Se sono riuscito a connettermi prendo le info
		printf("Id assegnato: %d\n",atoi(ansSplitted[1]));
		printMessage(ansSplitted[2]);
		return 1;
	}else{
		return 0;
	}
}

//Aspetta l'apertura della pipe del server
void waitServer(){
	// 1 - Cerco di aprire la pipe in scrittura
	server = open(S_PIPE_NAME, O_WRONLY);
	// 2 - Se la pipe non è ancora stata aperta, attendo e riprovo
	if(server == -1){ printf("Aspettando l'avvio del server"); }
	while(server == -1){
		printf(".");
		fflush(stdout);
		sleep(1);								//se fallisco aspetto 1s prima di ritentare		
		server = open(S_PIPE_NAME, O_WRONLY);	//apro in scrittura
		if(server > -1){ puts(""); }
	}
	// 3 - Finisco quando la pipe si è aperta
}


/* LIST */
void listRequest(){
	// 1 - Preparo il messaggio da trasmettere
	char mex[BUF_SIZE];
	createCommandStr(mex, LIST,"-1","");				//Creo il messaggio
	// 2 - Invio il messaggio e attendo la risposta
	sendRequest(mex);
	// 3 - La risposta è letta dall'handler, quindi ho finito
}



/* Terminazione */
//Chiude e rimuove la pipe in ingresso
void closeMyPipe(){
	close(myPipe);
	unlink(myPipeName);
}


/* Funzioni di appoggio varie */

//Crea un messaggio standard da inviare al server
int createCommandStr(char *str, int cod, char *dest, char *txt){
	return sprintf(str,"%d|%d|%s|%s",cod,myID,dest,txt);
}

//Invia un messaggio al server
int sendRequest(char *req){
	puts("Invio richiesta...");
	write(server, req, strlen(req)+1);		//scrivo sulla pipe
	//Mi metto in attesa di una risposta
	pause();
}

//Legge un messaggio dalla pipe in ingresso
int readLine(int fd, char *str){
	int n;
	// Legge un char alla volta fino allo \0 
	do{
		n = read(fd,str,1);	//legge un char
	}while(n>0 && *str++!='\0');
	return (n>0);	// ritorna false se si è già raggiunta EoF
}

//Suddivide la risposta in n elementi. l'ultimo è NULL
void splitAnswer(){
	//Cerco e salvo i pezzi di risposta
	const char s[2] = CMD_DELIM;
	char * tkn = strtok(answer,s);
	int i = 0;
	while(tkn != NULL){
		ansSplitted[i] = tkn;
		i++; 
		tkn = strtok(NULL, s);	//scorro alla prossima tkn
	}
	ansSplitted[i] = NULL;	//lLIENTS ATTIVI

}

//Stampa un messaggio
void printMessage(char *mex){
	printf("\n%s\n",mex);
	fflush(stdout);
	printf("Inserire l'operazione da eseguire: ");
	fflush(stdout);
}


/* SIGNAL HANDLER */
void incomingMessageHandler(int sig){
	char idAnswer[BUF_SIZE];
	while(!readLine(myPipe,answer)){	//Finchè non riesco a leggere
		sleep(1);						//Aspetto la risposta del server
	}
	//Elaboro la risposta
	splitAnswer();					//Divido la risposta in pezzi
	switch(atoi(ansSplitted[0])){
		case DENIAL:
			printMessage(ansSplitted[1]);	//Stampo il messaggio con l'errore
			break;
		case CONNECT:
			myID = atoi(ansSplitted[1]);	//Prendo l'id
			connected = 1;					//Aggiorno la flag
			break;
		case LIST:
			updates = 0;					//Aggiorno la flag
			printMessage(ansSplitted[1]);	
			break;
		case FORWARD:
			strcpy(idAnswer, ansSplitted[1]);
			printMessage(ansSplitted[2]);
			sendAnswer(idAnswer);
			break;
		case CONFIRM:
			printMessage(ansSplitted[1]);
			break;
		case KILL:
			closeMyPipe();
			puts("Bye!");
			raise(SIGKILL);	//harakiri
			break;
		
		case DISCONNECT:
			connected=0;
			myID=-1;
			printMessage(ansSplitted[1]);
			break;
	}
}


void updateHandler(int sig){
	updates = 1;
	puts("\n---> La lista dei clients connessi e' cambiata! <---");
	printf("Inserire l'operazione da eseguire: ");
	fflush(stdout);
}


void killHandler(int sig){
	disconnect();	
	closeMyPipe();
	puts("Bye!");
	raise(SIGKILL);	//harakiri
}

//invia un messaggio a un altro client
//tramite il server
void sendMessage(){
	
	char finalMex[BUF_SIZE];

	printf("Inserire l' ID del/dei client(s) a cui inviare il messaggio separati da ',' : ");
	fflush(stdout);
	char IDs[BUF_SIZE];
	readIDs(IDs);
	
	printf("Inserire il messaggio da inviare: ");
	fflush(stdout);
	char mex[BUF_SIZE];
	sprintf(mex,"%d -> ",myID);
	readMex(mex);

	createCommandStr(finalMex, FORWARD, IDs, mex);
	sendRequest(finalMex);


}

//invia la risposta per un messaggio ricevuto
//non usa sendRequest per non entrare in una pausa
//da cui rischia di non essere risvegliato
void sendAnswer(char * id){
	char finalMex[BUF_SIZE];
	char txt[BUF_SIZE];
	sprintf(txt, "%d -> Messaggio ricevuto!", myID);
	createCommandStr(finalMex, CONFIRM, id, txt);
 	write(server, finalMex, strlen(finalMex)+1); 	
}

char* readIDs(char * dest){
	char buf[BUF_SIZE ];
	int ctrl = read(0, buf, 1023);
      	buf[ctrl-1]='\0';
	return strcpy(dest, buf);	
}

char* readMex(char * dest){
	char buf [BUF_SIZE];
	int ctrl = read(0, buf, 1023);
	buf[ctrl-1]='\0';
	return strcat(dest, buf);
}


// chiede disconnessione del client dal server ma non lo termina
void disconnect(){
	char finalMex[BUF_SIZE];
	createCommandStr(finalMex, DISCONNECT, "-1", "");
	sendRequest(finalMex);	
}

/* TEST ONLY */
void killAll(){
	char mex[BUF_SIZE];
	createCommandStr(mex,KILL,"-1","Abort All!!");
	sendRequest(mex);
}
