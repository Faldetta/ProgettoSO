#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>
#include <ctype.h>


#define MAX_CLIENTS 10
#define S_PIPE_NAME "serverIN"
#define BUF_SIZE 1024

#define SERVER_WAIT_TIME 5		//Tempo di attesa del server tra una lettura della pipe e l'altra

#define CMD_DELIM "|"			//Delimitatore token comandi
#define ID_DELIM ","			//delimitatore token id destinatari multipli
#define INFO_DELIM "_"			//Delimitatore informazioni client

/* Definizione Codici Comandi */
#define DENIAL 0
#define CONFIRM 1
#define CONNECT 2
#define LIST 3
#define FORWARD 4
#define DISCONNECT 5
#define KILL 9			//Test only

/* SIGNALS */
#define SEND_MEX SIGUSR1 		//Notifica nuovo messaggio
#define SIG_UPDATE SIGUSR2		//Notifica aggiornamento clients


/* Strutture Dati Server */
typedef struct clnt{
	int pid;
	char *name;
	char *pipe;
	int id;
	char *info;	//stringa con le info del client da inviare agli altri
} Client;


typedef struct command{
	int cod;		//codice
	int id_sender;
	int *id_receivers;
	char *txt;
} Cmd;


Client **activeClients;	//Array clients online
int nClients = 0;		//Numero clients connessi
int myPipe;				//fd della pipe in ingresso al server
Cmd *currentCmd;		//Comando da servire


/* Prototipi */
void initServer();
void launchServer();

int readLine(int fd, char *str);
Cmd * saveCmd(char *rawCmd);
int * readReceivers(char *tkn);
int countToken(char *str, char delim);

void execCurrentCmd();
void freeCmd();
void sendAnswer(int id, char *mex);
void createAnswer(char *buf);

int connectNewClient();
int setClientID();
void denialOfService();
int saveClientInfo();
char * saveStr(char *str);
char * createInfoStr(int id);
void removeClient(int id);
void notifyAll(int id_sender);

void createClientsStr();

void killHandler(int sig);
void closeMyPipe();

void printCurrentCmd();
void printActiveClients();
void printClient();
void goodbyeCruelWorld();

void sendMessage(char * message);

/* MAIN */
int main(void){
	puts("SERVER");
	puts("Initializing structures...");
	initServer();
	puts("Launching...");
	launchServer();
}


//Inizializza le strutture dati
void initServer(){
	activeClients = (Client **)calloc(MAX_CLIENTS+1,sizeof(Client *));	//L'ultimo slot è di appoggio
	//Creo la pipe:
	unlink(S_PIPE_NAME);						//rimuove la pipe se già presente, per chiudere altre connessioni
	mknod(S_PIPE_NAME,S_IFIFO, 0);				//creo la pipe
	chmod(S_PIPE_NAME, 0666);					//modifico i permessi della pipe
	//apro la pipe in lettura (non bloccante, almeno non aspetto che i client la aprano in scrittura)
	myPipe = open(S_PIPE_NAME, O_RDONLY|O_NONBLOCK);	
	signal(SIGINT,killHandler);		//Imposto l'handler per le terminazioni da tastiera
}


//Fa lavorare il server
void launchServer(){
	char buf[BUF_SIZE];	//Buffer di appoggio per la lettura di un comando
	int nc;				//numero di caratteri
	/* Ciclo infinito */
	puts("Ready!");
	while(1){
		// puts("Reading from pipe_IN...");
		nc = readLine(myPipe,buf);
		if(nc){
			puts("New Command found!");
			puts("Saving Command...");
			// 1 - Salvo il comando
			currentCmd = saveCmd(buf);			
			if(currentCmd == NULL){ 			// notifico errore
				currentCmd->cod = DENIAL;
				sprintf(currentCmd->txt,"WARNING: Error 400 - Bad Request.");
				execCurrentCmd();
			}			
			printCurrentCmd();					/* test */
			// 2 - Eseguo il comando
			puts("Processing command...");
			execCurrentCmd();
			puts("Done!");			
			// 3 - Libero la memoria
			freeCmd(currentCmd);

			sleep(1);							//Per evitare sovrapposizioni se 2 comandi di seguito sono rivolti allo stesso client
		}else{
			sleep(SERVER_WAIT_TIME);			//per evitare sovraccarico
		}
	}
}

/* Funzioni di lettura e traduzione Comandi */

//Legge un comando dalla pipe
int readLine(int fd, char *str){
	int n;
	// Legge un char alla volta fino allo \0 
	do{
		n = read(fd,str,1);	//legge un char
	}while(n>0 && *str++!='\0');

	return (n>0);	// ritorna false se si è già raggiunta EoF
}


//Crea un comando, return NULL in caso di errore
Cmd * saveCmd(char *rawCmd){
	Cmd *cptr = (Cmd *)malloc(sizeof(Cmd));
	const char s[2] = CMD_DELIM;
	char * tkn = strtok(rawCmd,s);
	int counter = 0;
	char * strRec;		//stringa temporanea, di appoggio
	while(tkn != NULL){
		switch(counter){
			case 0:
				cptr->cod = atoi(tkn);
				break;
			case 1:
				cptr->id_sender = atoi(tkn);
				break;
			case 2:
				strRec = tkn;	//Devo salvare la stringa e convertirla dopo, perchè strtok altrimenti non funziona <-------------oppure usare fseek??--------
				break;
			case 3:
				cptr->txt = tkn;
				break;
			default:
				printf("Error: too many arguments!\ntkn: %s\n",tkn);	/* test */
		}
			counter++;
			tkn = strtok(NULL, s);	//scorro alla prossima tkn
	}
	cptr->id_receivers = readReceivers(strRec);	//la stringa con gli id destinatari viene messa in un array
	if(cptr->id_receivers == NULL){
		puts("Errore! Comando non valido!");
		free(cptr);
		return NULL;
	}
	return cptr;
}


//crea array con id client destinatari(separati da ID_DELIM) 
//--> ritorna NULL in caso di errore
int * readReceivers(char *tkn){
	const char s[2] = ID_DELIM;
	//Conto i delimitatori per contare i destinatari 
	int nDest = countToken(tkn,s[0]);
	nDest += 2; // I destinatari sono n+2: uno per l'ultima posizione e uno per il -10 finale
	//creo l'array:
	int *receivers = (int *)malloc(nDest*sizeof(int));
	//Inserisco gli id
	char *ntk = strtok(tkn,s);
	int i=0;
	while(ntk!=NULL){
		if(isdigit(*ntk) || *ntk=='-'){
			receivers[i] = atoi(ntk);
			i++;
		}else{
			puts("Errore! ID non valido!");
			free(receivers);
			return NULL;
		}
		ntk = strtok(NULL, s);	//scorro alla prossima ntk
	}
	receivers[i] = -10;	//l'ultimo elemento è -10, per riconoscere la fine
	return(receivers);
}	


//Conta il numero di token
int countToken(char *str, char delim){
	int len = strlen(str);	//lunghezza stringa
	int i, nDelim=0;		
	for(i=0;i<len;i++){
		if(str[i]==delim){
			nDelim++;
		}
	}
	return nDelim;
}


/* Funzioni per esecuzione dei comandi */

//Esegue i controlli sul comando e chiama l'esecutore
void execCurrentCmd(){
	char answer[BUF_SIZE];
	//switch in base al codice comando
	switch(currentCmd->cod){
		
		case DENIAL:
			createAnswer(answer);
			sendAnswer(currentCmd->id_sender,answer);
			break;
	
		case CONNECT:
			connectNewClient();
			break;
	
		case LIST:
			createClientsStr();							
			createAnswer(answer);
			sendAnswer(currentCmd->id_sender,answer);
			break;
	
		case FORWARD:
			createAnswer(answer);
			sendMessage(answer);
			break;

		case CONFIRM:
			createAnswer(answer);
			sendAnswer(currentCmd->id_receivers[0], answer);
			break;	

		case DISCONNECT:
			createAnswer(answer);
			if(nClients>1){ notifyAll(currentCmd->id_sender); }
			sendAnswer(currentCmd->id_sender, answer);
			removeClient(currentCmd->id_sender);
			break;
			
		case KILL:
			goodbyeCruelWorld();
			break;
	
		default:
			printActiveClients();			/* Test */
	}
}

//Libero il comando
void freeCmd(){
	free(currentCmd->id_receivers);
	free(currentCmd);
	currentCmd = NULL;
}

//Invia un messaggio ad un client
void sendAnswer(int id, char *mex){
	int cfd = open(activeClients[id]->pipe, O_WRONLY);	//apro in scrittura	(assumo lato lettura sempre aperto)
	kill(activeClients[id]->pid, SEND_MEX);				//Notifica il nuovo messaggio
	write(cfd, mex, strlen(mex)+1);						//scrivo sulla pipe
	close(cfd);											//chiudo la pipe
}

//Crea una risposta predefinita in base al comando
void createAnswer(char *buf){
	char tmp[BUF_SIZE];
	switch(currentCmd->cod){

		case DENIAL:
			sprintf(buf,"%d|%s",DENIAL,currentCmd->txt);
			break;

		case CONNECT:
			sprintf(buf,"%d|%d|%s",CONNECT,currentCmd->id_sender,currentCmd->txt);
			break;

		case LIST:
			sprintf(buf,"%d|%s",LIST,currentCmd->txt);
			break;

		case FORWARD:
			sprintf(buf, "%d|%d|%s",FORWARD, currentCmd->id_sender, currentCmd->txt);
			break;

		case CONFIRM:
			sprintf(buf, "%d|%s", CONFIRM, currentCmd->txt);
			break;

		case DISCONNECT:
			sprintf(buf, "%d|%s",DISCONNECT, "Client Disconnesso");
			break;

	}
}


/* 1 - Connection */
//Gestisce la connessione di un nuovo client. Return 1=success, 0=fail!
int connectNewClient(){
	puts("New Connection Request!\nLooking for a new ID...");	/* test */	
	// 1 - Assegno ID
	int cid = setClientID();		// Assegno un ID al client 
	if(cid<0){						// Se non ho ID disponibili
		puts("No ID available...\nSending Denial answer...");
		denialOfService();			// Segnalo al client di non poterlo connettere
		puts("Done!");
		return 0;
	}else{
		puts("Saving new client...");
		// 2 - Salvo le info del client
		saveClientInfo();
		puts("Sending answer...");
		// 3 - Invio messaggio di conferma connessione + lista utenti
		nClients++;								//aggiorno
		char ans[BUF_SIZE];
		createClientsStr();						// creo la lista utenti
		createAnswer(ans);
		sendAnswer(currentCmd->id_sender,ans);
		// 4 - Se ci sono altri client
		if(nClients>1){	
			puts("Notify other clients...");
			// 5 - Notifico un cambiamento a tutti i client
			notifyAll(currentCmd->id_sender);
		}
		puts("Done!");		
		return 1;						
	}							
}

//Return -1 if IDnotFound
int setClientID(){
	//Cerca id disponibile
	int i;
	for(i=0;i<=MAX_CLIENTS;i++){
		if(activeClients[i] == NULL){								//Se trovo un id libero
			currentCmd->id_sender = i;								//Inserisce l'id nel comando corrente
			currentCmd->id_receivers[0] = i;						//Anche nel destinatario
			activeClients[i] = (Client *)calloc(1,sizeof(Client));	//Creo lo spazio per un nuovo client
			activeClients[i]->id = i;								//Inserisco l'ID (anche se è ridondante)	<-------------------------------------
			if(i==MAX_CLIENTS){ return -1;}							//ID temporaneo per denial
			return 1;												
		}
	}
}


//In caso di mancanza di ID disponibili
void denialOfService(){
	// 1 - Salvo temporaneamente il client
	saveClientInfo();	
	// 2 - Modifico il comando, per segnalare l'errore
	currentCmd->cod = DENIAL;										//imposto il codice
	sprintf(currentCmd->txt,"WARNING: Error 503 - Service Unavailable.");	//Modifico il messaggio
	// 3 - Invio messaggio di errore
	execCurrentCmd();												//Faccio eseguire il messaggio
	// 4 - Ripristino
	removeClient(MAX_CLIENTS);										
}


//Prende le info del client e le salva	<--------------------------------------------------------------------(assume che le info siano corrette)--------------
int saveClientInfo(){
	const char s[2] = INFO_DELIM;
	char *tkn = strtok(currentCmd->txt,s);
	int count = 0;
	while(tkn != NULL){
		switch(count){
			case 0:		
				activeClients[currentCmd->id_sender]->pid = atoi(tkn);		//pid
				break;
			case 1:
				activeClients[currentCmd->id_sender]->name = saveStr(tkn);	//Name
				break;
			case 2:
				activeClients[currentCmd->id_sender]->pipe = saveStr(tkn);	//Pipe
				break;
		}
		count++;
		tkn = strtok(NULL, s);	//scorro alla prossima tkn
	}
	activeClients[currentCmd->id_sender]->info = createInfoStr(currentCmd->id_sender); //Info 
	return 1;	//assumendo che non ci siano errori nella stringa in ingresso non ritorna altro	<-------------------------------------------------------
}	


//Crea una copia della stringa in modo che sia salvata in memoria	//<------------------------------------------------------necessario??----------------
char * saveStr(char *str){
	int len = strlen(str);
	len++;	//aggiungo lo \0 finale
	char *hardCopy = (char *)malloc(len*sizeof(char));
	return strcpy(hardCopy,str);
}


//Crea un stringa pronta per la stampa con le info del client
char * createInfoStr(int id){
	char tmp[BUF_SIZE];
	tmp[0] = '\0';	
	sprintf(tmp,"Client - ID: %d\tName: %s\n",activeClients[id]->id,activeClients[id]->name);
	return saveStr(tmp);
}	




//Rimuove un client dalla lista, liberando la memoria
void removeClient(int id){
	free(activeClients[id]->name);
	free(activeClients[id]->pipe);
	free(activeClients[id]->info);
	free(activeClients[id]);
	activeClients[id] = NULL;
	nClients--;
}


//Notifica un cambiamento nella lista degli id attivi a tutti i clients
//Tranne che al client responsabile del cambiamento
void notifyAll(int id_sender){
	int i;
	for(i=0;i<MAX_CLIENTS;i++){
		if(activeClients[i]!=NULL && i!=id_sender){
			kill(activeClients[i]->pid,SIG_UPDATE);
		}
	}
}

/* 2 - LISTA UTENTI */
//Crea la lista degli utenti connessi (escluso il richiedente) e la inserisce in cmd->txt		//Rinominare funzione<-----------------------------------------
void createClientsStr(){
	int i;
	sprintf(currentCmd->txt,"\nCLIENTS ATTIVI\n");
	if(nClients>1){
		for(i=0;i<MAX_CLIENTS;i++){
			if(activeClients[i]!=NULL && i!=currentCmd->id_sender){	//non inserisco il richiedente
				strcat(currentCmd->txt,activeClients[i]->info);
			}
		}	
	}else{
		strcat(currentCmd->txt,"Non ci sono altri utenti connessi!\n");
	}	
}



/* HANDLER */
void killHandler(int sig){
	closeMyPipe();
	int i;
	//Invio segnale per uccidere tutti i client
	for(i=0;i<MAX_CLIENTS;i++){
		if(activeClients[i]!=NULL){
			kill(activeClients[i]->pid,SIGINT);	
		}	
	}
	puts("Bye!");
	raise(SIGKILL);	//harakiri
}

//Chiude e rimuove la pipe in ingresso
void closeMyPipe(){
	close(myPipe);
	unlink(S_PIPE_NAME);
}

/* TEST */
void printCurrentCmd(){
	puts("COMANDO");
	printf("Codice Comando: %d\n",currentCmd->cod);
	printf("Id richiedente: %d\n",currentCmd->id_sender);
	printf("Id destinatari: ");
	int i=0;
	while(currentCmd->id_receivers[i] != -10){
		printf("%d ", currentCmd->id_receivers[i]);
		i++;
	}
	printf("\nTesto del comando: %s\n",currentCmd->txt);
}

void printActiveClients(){
	int i=0;
	puts("ACTIVE CLIENTS");			
	for(;i<MAX_CLIENTS;i++){
		if(activeClients[i]!=NULL)
			printClient(i);
	}
	puts("");
}

void printClient(int id){
	puts("--Client--");
	printf("PID: %d\n", activeClients[id]->pid);
	printf("Name: %s\n", activeClients[id]->name);
	printf("Pipe: %s\n", activeClients[id]->pipe);
	printf("ID: %d\n", activeClients[id]->id);
	printf("Info: %s\n", activeClients[id]->info);
}


void goodbyeCruelWorld(){
	//Chiudo ed elimino la pipe
	closeMyPipe();
	//Uccido tutti i clients
	int i=0;
	char mex[2] = "9";
	for(;i<MAX_CLIENTS;i++){
		if(activeClients[i]!=NULL){
			sendAnswer(i,mex);
		}
	}
	//Harakiri
	raise(SIGKILL);
}

//Invia il messaggio di uno dei client
//ai riceventi sfruttando sendAnswer
void sendMessage(char * message){
	int * receivers;
	receivers=currentCmd->id_receivers;
	int i;
	char errorMessage[BUF_SIZE];
	for(i=0; receivers[i] != -10; i++){
		if(activeClients[receivers[i]]!=NULL){
            sendAnswer(receivers[i], message);
        }else{
	        sprintf(errorMessage, "%d|%s : %d", DENIAL, "WARNING - Client inesistente",receivers[i]);
	        sendAnswer(currentCmd->id_sender,errorMessage);
    	}
	}
}

